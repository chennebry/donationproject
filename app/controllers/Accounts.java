package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Accounts extends Controller {

	public static void index() {
		render();
	}

	public static void signup() {
		List<String> countries = Arrays.asList("Afghanistan", "Albania",
				"Algeria", "American Samoa", "Andorra", "Angola", "Anguilla",
				"Antarctica", "Antigua and Barbuda", "Argentina", "Armenia",
				"Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas",
				"Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
				"Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
				"Bosnia and Herzegowina", "Botswana", "Bouvet Island",
				"Brazil", "British Indian Ocean Territory",
				"Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi",
				"Cambodia", "Cameroon", "Canada", "Cape Verde",
				"Cayman Islands", "Central African Republic", "Chad", "Chile",
				"China", "Christmas Island", "Cocos (Keeling) Islands",
				"Colombia", "Comoros", "Congo",
				"Congo, the Democratic Republic of the", "Cook Islands",
				"Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba",
				"Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
				"Dominican Republic", "East Timor", "Ecuador", "Egypt",
				"El Salvador", "Equatorial Guinea", "Eritrea", "Estonia",
				"Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands",
				"Fiji", "Finland", "France", "France Metropolitan",
				"French Guiana", "French Polynesia",
				"French Southern Territories", "Gabon", "Gambia", "Georgia",
				"Germany", "Ghana", "Gibraltar", "Greece", "Greenland",
				"Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea",
				"Guinea-Bissau", "Guyana", "Haiti",
				"Heard and Mc Donald Islands", "Holy See (Vatican City State)",
				"Honduras", "Hong Kong", "Hungary", "Iceland", "India",
				"Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland",
				"Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan",
				"Kenya", "Kiribati", "Korea, Democratic People's Republic of",
				"Korea, Republic of", "Kuwait", "Kyrgyzstan",
				"Lao, People's Democratic Republic", "Latvia", "Lebanon",
				"Lesotho", "Liberia", "Libyan Arab Jamahiriya",
				"Liechtenstein", "Lithuania", "Luxembourg", "Macau",
				"Macedonia, The Former Yugoslav Republic of", "Madagascar",
				"Malawi", "Malaysia", "Maldives", "Mali", "Malta",
				"Marshall Islands", "Martinique", "Mauritania", "Mauritius",
				"Mayotte", "Mexico", "Micronesia, Federated States of",
				"Moldova, Republic of", "Monaco", "Mongolia", "Montserrat",
				"Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru",
				"Nepal", "Netherlands", "Netherlands Antilles",
				"New Caledonia", "New Zealand", "Nicaragua", "Niger",
				"Nigeria", "Niue", "Norfolk Island",
				"Northern Mariana Islands", "Norway", "Oman", "Pakistan",
				"Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru",
				"Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico",
				"Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda",
				"Saint Kitts and Nevis", "Saint Lucia",
				"Saint Vincent and the Grenadines", "Samoa", "San Marino",
				"Sao Tome and Principe", "Saudi Arabia", "Senegal",
				"Seychelles", "Sierra Leone", "Singapore",
				"Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands",
				"Somalia", "South Africa",
				"South Georgia and the South Sandwich Islands", "Spain",
				"Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan",
				"Suriname", "Svalbard and Jan Mayen Islands", "Swaziland",
				"Sweden", "Switzerland", "Syrian Arab Republic",
				"Taiwan, Province of China", "Tajikistan",
				"Tanzania, United Republic of", "Thailand", "Togo", "Tokelau",
				"Tonga", "Trinidad and Tobago", "Tunisia", "Turkey",
				"Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda",
				"Ukraine", "United Arab Emirates", "United Kingdom",
				"United States", "United States Minor Outlying Islands",
				"Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam",
				"Virgin Islands (British)", "Virgin Islands (U.S.)",
				"Wallis and Futuna Islands", "Western Sahara", "Yemen",
				"Yugoslavia", "Zambia", "Zimbabwe");
		render(countries);
	}

	public static void register(boolean ukCitizen, String firstName,
			String lastName, String email, String password, int age,
			String addressLine1, String addressLine2, String town,
			String state, String country, String postalCode, String house) {
		Logger.info(ukCitizen + " " + firstName + " " + lastName + " " + email
				+ " " + password + " " + age + " " + house);

		User user = new User(ukCitizen, firstName, lastName, email, password,
				age, addressLine1, addressLine2, town, state, country,
				postalCode, house);

		user.save();

		DonationController.index();
	}

	public static void login() {
		render();
	}

	public static void logout() {

		session.clear();
		index();
	}

	public static void authenticate(String email, String password) {
		Logger.info("Attempting to authenticate with " + email + ":" + password);

		User user = User.findByEmail(email);
		if ((user != null) && (user.checkPassword(password) == true)) {
			Logger.info("Successfull authentication of  " + user.firstName
					+ " " + user.lastName);
			session.put("logged_in_userid", user.id);

			if (user.email.equals("admin")) {
				Admin.index();
			}

			else {
				DonationController.index();
			}
		} else {
			Logger.info("Authentication failed");
			login();
		}
	}

	public static User getCurrentUser() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			return null;
		}
		User logged_in_user = User.findById(Long.parseLong(userId));
		Logger.info("In Accounts controller: Logged in user is "
				+ logged_in_user.firstName);
		return logged_in_user;
	}
}
