package controllers;

import play.Logger;
import play.db.jpa.JPA;
import play.mvc.*;

import java.util.*;

import javax.persistence.Query;

import models.*;

public class DonationController extends Controller {
	public static void index() {
		User user = Accounts.getCurrentUser();
		List<Donation> candidates = Candidate.findAll();
		if (user == null) {
			Accounts.login();
		} else {
			render(user, candidates);
		}
	}

	public static void visit(long id) {
		User user = Accounts.getCurrentUser();
		List<Donation> candidates = Candidate.findAll();
		if (user == null) {
			Accounts.login();
		} else {

			Candidate candidate = Candidate.findById(id);
			String prog = getPercentTargetAchieved(id);
			String progress = prog + "%";
			Logger.info("Donation ctrler : user is " + user.email);
			Logger.info("Donation ctrler : percent target achieved " + progress);
			render(user, candidate, candidates, progress);
		}
	}

	/**
	 * Log and save to database amount donated and method of donation, eg.
	 * paypal, direct payment
	 * 
	 * @param amountDonated
	 * @param methodDonated
	 */
	public static void donate(long amountDonated, String methodDonated,
			String candidateId) {
		Logger.info("amount donated " + amountDonated + " " + "method donated "
				+ methodDonated);

		User user = Accounts.getCurrentUser();
		Candidate candidate = Candidate.findById(Long.parseLong(candidateId));
		if (user == null) {
			Logger.info("Donation class : Unable to getCurrentuser");
			Accounts.login();
		} else {
			addDonation(user, amountDonated, methodDonated, candidate);
		}
		index();
	}

	/**
	 * @param user
	 * @param amountDonated
	 */
	private static void addDonation(User user, long amountDonated,
			String methodDonated, Candidate candidate) {
		Donation bal = new Donation(user, amountDonated, methodDonated,
				candidate);
		bal.save();
	}

	// private static long getDonationTarget()
	// {
	// List<Candidate> candidates = Candidate.findAll();
	// long target = candidates.target;
	// return target;
	// }

	public static String getPercentTargetAchieved(long id) {

		Candidate candidate = Candidate.findById(id);
		List<Donation> allDonations = Donation.findAll();
		long total = 0;
		for (Donation donation : allDonations) {
			if (donation.to == candidate) {
				total += donation.received;
			}

		}
		long target = candidate.target;
		long percentachieved = (total * 100 / target);
		String progress = String.valueOf(percentachieved);
		Logger.info("Percent of target achieved (string) " + progress
				+ " percentachieved (long)= " + percentachieved);
		return progress;
	}

	public static void report() {
		List<Donation> candidates = Candidate.findAll();
		List<String> countries = Arrays.asList("Afghanistan", "Albania",
				"Algeria", "American Samoa", "Andorra", "Angola", "Anguilla",
				"Antarctica", "Antigua and Barbuda", "Argentina", "Armenia",
				"Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas",
				"Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
				"Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
				"Bosnia and Herzegowina", "Botswana", "Bouvet Island",
				"Brazil", "British Indian Ocean Territory",
				"Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi",
				"Cambodia", "Cameroon", "Canada", "Cape Verde",
				"Cayman Islands", "Central African Republic", "Chad", "Chile",
				"China", "Christmas Island", "Cocos (Keeling) Islands",
				"Colombia", "Comoros", "Congo",
				"Congo, the Democratic Republic of the", "Cook Islands",
				"Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba",
				"Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
				"Dominican Republic", "East Timor", "Ecuador", "Egypt",
				"El Salvador", "Equatorial Guinea", "Eritrea", "Estonia",
				"Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands",
				"Fiji", "Finland", "France", "France Metropolitan",
				"French Guiana", "French Polynesia",
				"French Southern Territories", "Gabon", "Gambia", "Georgia",
				"Germany", "Ghana", "Gibraltar", "Greece", "Greenland",
				"Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea",
				"Guinea-Bissau", "Guyana", "Haiti",
				"Heard and Mc Donald Islands", "Holy See (Vatican City State)",
				"Honduras", "Hong Kong", "Hungary", "Iceland", "India",
				"Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland",
				"Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan",
				"Kenya", "Kiribati", "Korea, Democratic People's Republic of",
				"Korea, Republic of", "Kuwait", "Kyrgyzstan",
				"Lao, People's Democratic Republic", "Latvia", "Lebanon",
				"Lesotho", "Liberia", "Libyan Arab Jamahiriya",
				"Liechtenstein", "Lithuania", "Luxembourg", "Macau",
				"Macedonia, The Former Yugoslav Republic of", "Madagascar",
				"Malawi", "Malaysia", "Maldives", "Mali", "Malta",
				"Marshall Islands", "Martinique", "Mauritania", "Mauritius",
				"Mayotte", "Mexico", "Micronesia, Federated States of",
				"Moldova, Republic of", "Monaco", "Mongolia", "Montserrat",
				"Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru",
				"Nepal", "Netherlands", "Netherlands Antilles",
				"New Caledonia", "New Zealand", "Nicaragua", "Niger",
				"Nigeria", "Niue", "Norfolk Island",
				"Northern Mariana Islands", "Norway", "Oman", "Pakistan",
				"Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru",
				"Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico",
				"Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda",
				"Saint Kitts and Nevis", "Saint Lucia",
				"Saint Vincent and the Grenadines", "Samoa", "San Marino",
				"Sao Tome and Principe", "Saudi Arabia", "Senegal",
				"Seychelles", "Sierra Leone", "Singapore",
				"Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands",
				"Somalia", "South Africa",
				"South Georgia and the South Sandwich Islands", "Spain",
				"Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan",
				"Suriname", "Svalbard and Jan Mayen Islands", "Swaziland",
				"Sweden", "Switzerland", "Syrian Arab Republic",
				"Taiwan, Province of China", "Tajikistan",
				"Tanzania, United Republic of", "Thailand", "Togo", "Tokelau",
				"Tonga", "Trinidad and Tobago", "Tunisia", "Turkey",
				"Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda",
				"Ukraine", "United Arab Emirates", "United Kingdom",
				"United States", "United States Minor Outlying Islands",
				"Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam",
				"Virgin Islands (British)", "Virgin Islands (U.S.)",
				"Wallis and Futuna Islands", "Western Sahara", "Yemen",
				"Yugoslavia", "Zambia", "Zimbabwe");
		render(countries, candidates);
	}

	public static void byAge(String ageRange) {
		Logger.info("Show range :" + ageRange);
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		List<Donation> donationSorted = new ArrayList<Donation>();
		List<Donation> donations = Donation.findAll();

		if (ageRange.equals("val1")) {
			int from = 16;
			int to = 24;

			for (int i = 0; i < donations.size(); i++) {
				if ((donations.get(i).from.age >= from)
						&& (donations.get(i).from.age <= to)) {
					donationSorted.add(donations.get(i));
				}
			}
		}

		if (ageRange.equals("val2")) {
			int from = 25;
			int to = 34;

			for (int i = 0; i < donations.size(); i++) {
				if ((donations.get(i).from.age >= from)
						&& (donations.get(i).from.age <= to)) {
					donationSorted.add(donations.get(i));
				}
			}
		}

		if (ageRange.equals("val3")) {
			int from = 35;
			int to = 44;

			for (int i = 0; i < donations.size(); i++) {
				if ((donations.get(i).from.age >= from)
						&& (donations.get(i).from.age <= to)) {
					donationSorted.add(donations.get(i));
				}
			}
		}

		if (ageRange.equals("val4")) {
			int from = 45;
			int to = 54;

			for (int i = 0; i < donations.size(); i++) {
				if ((donations.get(i).from.age >= from)
						&& (donations.get(i).from.age <= to)) {
					donationSorted.add(donations.get(i));
				}
			}
		}

		if (ageRange.equals("val5")) {
			int from = 55;
			int to = 64;

			for (int i = 0; i < donations.size(); i++) {
				if ((donations.get(i).from.age >= from)
						&& (donations.get(i).from.age <= to)) {
					donationSorted.add(donations.get(i));
				}
			}
		}

		if (ageRange.equals("val6")) {
			int from = 65;
			int to = 74;

			for (int i = 0; i < donations.size(); i++) {
				if ((donations.get(i).from.age >= from)
						&& (donations.get(i).from.age <= to)) {
					donationSorted.add(donations.get(i));
				}
			}
		}

		if (ageRange.equals("val7")) {
			int from = 75;

			for (int i = 0; i < donations.size(); i++) {
				if (donations.get(i).from.age >= from) {
					donationSorted.add(donations.get(i));
				}
			}
		}

		render("DonationController/renderReport.html", user, donationSorted);
	}

	public static void byCountry(String country) {
		Logger.info("Show range :" + country);
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		List<Donation> donationSorted = new ArrayList<Donation>();
		List<Donation> donations = Donation.findAll();
		List<String> countries = Arrays.asList("Afghanistan", "Albania",
				"Algeria", "American Samoa", "Andorra", "Angola", "Anguilla",
				"Antarctica", "Antigua and Barbuda", "Argentina", "Armenia",
				"Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas",
				"Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
				"Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
				"Bosnia and Herzegowina", "Botswana", "Bouvet Island",
				"Brazil", "British Indian Ocean Territory",
				"Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi",
				"Cambodia", "Cameroon", "Canada", "Cape Verde",
				"Cayman Islands", "Central African Republic", "Chad", "Chile",
				"China", "Christmas Island", "Cocos (Keeling) Islands",
				"Colombia", "Comoros", "Congo",
				"Congo, the Democratic Republic of the", "Cook Islands",
				"Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba",
				"Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
				"Dominican Republic", "East Timor", "Ecuador", "Egypt",
				"El Salvador", "Equatorial Guinea", "Eritrea", "Estonia",
				"Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands",
				"Fiji", "Finland", "France", "France Metropolitan",
				"French Guiana", "French Polynesia",
				"French Southern Territories", "Gabon", "Gambia", "Georgia",
				"Germany", "Ghana", "Gibraltar", "Greece", "Greenland",
				"Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea",
				"Guinea-Bissau", "Guyana", "Haiti",
				"Heard and Mc Donald Islands", "Holy See (Vatican City State)",
				"Honduras", "Hong Kong", "Hungary", "Iceland", "India",
				"Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland",
				"Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan",
				"Kenya", "Kiribati", "Korea, Democratic People's Republic of",
				"Korea, Republic of", "Kuwait", "Kyrgyzstan",
				"Lao, People's Democratic Republic", "Latvia", "Lebanon",
				"Lesotho", "Liberia", "Libyan Arab Jamahiriya",
				"Liechtenstein", "Lithuania", "Luxembourg", "Macau",
				"Macedonia, The Former Yugoslav Republic of", "Madagascar",
				"Malawi", "Malaysia", "Maldives", "Mali", "Malta",
				"Marshall Islands", "Martinique", "Mauritania", "Mauritius",
				"Mayotte", "Mexico", "Micronesia, Federated States of",
				"Moldova, Republic of", "Monaco", "Mongolia", "Montserrat",
				"Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru",
				"Nepal", "Netherlands", "Netherlands Antilles",
				"New Caledonia", "New Zealand", "Nicaragua", "Niger",
				"Nigeria", "Niue", "Norfolk Island",
				"Northern Mariana Islands", "Norway", "Oman", "Pakistan",
				"Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru",
				"Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico",
				"Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda",
				"Saint Kitts and Nevis", "Saint Lucia",
				"Saint Vincent and the Grenadines", "Samoa", "San Marino",
				"Sao Tome and Principe", "Saudi Arabia", "Senegal",
				"Seychelles", "Sierra Leone", "Singapore",
				"Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands",
				"Somalia", "South Africa",
				"South Georgia and the South Sandwich Islands", "Spain",
				"Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan",
				"Suriname", "Svalbard and Jan Mayen Islands", "Swaziland",
				"Sweden", "Switzerland", "Syrian Arab Republic",
				"Taiwan, Province of China", "Tajikistan",
				"Tanzania, United Republic of", "Thailand", "Togo", "Tokelau",
				"Tonga", "Trinidad and Tobago", "Tunisia", "Turkey",
				"Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda",
				"Ukraine", "United Arab Emirates", "United Kingdom",
				"United States", "United States Minor Outlying Islands",
				"Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam",
				"Virgin Islands (British)", "Virgin Islands (U.S.)",
				"Wallis and Futuna Islands", "Western Sahara", "Yemen",
				"Yugoslavia", "Zambia", "Zimbabwe");
		for (int i = 0; i < donations.size(); i++) {
			if (donations.get(i).from.country.equals(country)) {
				donationSorted.add(donations.get(i));
			}
		}

		render("DonationController/renderReport.html", user, countries,
				donationSorted);
	}

	public static void byCandidate(String candidateId) {
		Logger.info("Show candidate :" + candidateId);
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		Candidate candidate = Candidate.findById(Long.parseLong(candidateId));

		List<Donation> donationSorted = new ArrayList<Donation>();
		List<Donation> donations = Donation.findAll();
		List<Donation> candidates = Candidate.findAll();

		for (int i = 0; i < donations.size(); i++) {
			if (donations.get(i).to.id.equals(candidate.id)) {
				donationSorted.add(donations.get(i));
				Logger.info("Candidate is" + candidate);
			}
		}

		render("DonationController/renderReport.html", user, candidates,
				donationSorted);

	}

	public static void byOffice(String office) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		List<Donation> donationSorted = new ArrayList<Donation>();
		List<Donation> donations = Donation.findAll();
		List<Donation> candidates = Candidate.findAll();

		for (int i = 0; i < donations.size(); i++) {
			if (donations.get(i).to.office.equals(office)) {
				donationSorted.add(donations.get(i));
				Logger.info("Office is" + office);
			}
		}
		render("DonationController/renderReport.html", user, candidates,
				donationSorted);
	}

	public static void byHouse(String house) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		List<Donation> donationSorted = new ArrayList<Donation>();
		List<Donation> donations = Donation.findAll();
		List<Donation> candidates = Candidate.findAll();

		for (int i = 0; i < donations.size(); i++) {
			if (donations.get(i).from.house.equals(house)) {
				donationSorted.add(donations.get(i));
				Logger.info("House is" + house);
			}
		}

		render("DonationController/renderReport.html", user, candidates,
				donationSorted);

	}
}