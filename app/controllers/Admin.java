package controllers;

import play.Logger;
import play.db.jpa.JPA;
import play.mvc.*;

import java.util.*;

import javax.persistence.Query;

import models.*;

public class Admin extends Controller {
	public static void index() {
		render();
	}

	public static void addCandidate() {
		List<Donation> candidates = Candidate.findAll();
		render(candidates);
	}

	public static void register(String firstName, String lastName,
			String office, String message, long target) {
		Logger.info(firstName + " " + lastName + " " + office + " " + target);

		Candidate candidate = new Candidate(firstName, lastName, office,
				message, target);

		candidate.save();

		index();
	}

	public static void report() {
		List<Candidate> candidates = Candidate.findAll();
		List<Donation> donations = Donation.findAll();
		List<User> users = User.findAll();
		render(candidates, donations, users);
	}
}