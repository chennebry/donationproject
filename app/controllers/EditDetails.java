package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class EditDetails extends Controller {
	public static void index() {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		List<String> countries = Arrays.asList("Afghanistan", "Albania",
				"Algeria", "American Samoa", "Andorra", "Angola", "Anguilla",
				"Antarctica", "Antigua and Barbuda", "Argentina", "Armenia",
				"Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas",
				"Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
				"Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
				"Bosnia and Herzegowina", "Botswana", "Bouvet Island",
				"Brazil", "British Indian Ocean Territory",
				"Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi",
				"Cambodia", "Cameroon", "Canada", "Cape Verde",
				"Cayman Islands", "Central African Republic", "Chad", "Chile",
				"China", "Christmas Island", "Cocos (Keeling) Islands",
				"Colombia", "Comoros", "Congo",
				"Congo, the Democratic Republic of the", "Cook Islands",
				"Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba",
				"Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
				"Dominican Republic", "East Timor", "Ecuador", "Egypt",
				"El Salvador", "Equatorial Guinea", "Eritrea", "Estonia",
				"Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands",
				"Fiji", "Finland", "France", "France Metropolitan",
				"French Guiana", "French Polynesia",
				"French Southern Territories", "Gabon", "Gambia", "Georgia",
				"Germany", "Ghana", "Gibraltar", "Greece", "Greenland",
				"Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea",
				"Guinea-Bissau", "Guyana", "Haiti",
				"Heard and Mc Donald Islands", "Holy See (Vatican City State)",
				"Honduras", "Hong Kong", "Hungary", "Iceland", "India",
				"Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland",
				"Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan",
				"Kenya", "Kiribati", "Korea, Democratic People's Republic of",
				"Korea, Republic of", "Kuwait", "Kyrgyzstan",
				"Lao, People's Democratic Republic", "Latvia", "Lebanon",
				"Lesotho", "Liberia", "Libyan Arab Jamahiriya",
				"Liechtenstein", "Lithuania", "Luxembourg", "Macau",
				"Macedonia, The Former Yugoslav Republic of", "Madagascar",
				"Malawi", "Malaysia", "Maldives", "Mali", "Malta",
				"Marshall Islands", "Martinique", "Mauritania", "Mauritius",
				"Mayotte", "Mexico", "Micronesia, Federated States of",
				"Moldova, Republic of", "Monaco", "Mongolia", "Montserrat",
				"Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru",
				"Nepal", "Netherlands", "Netherlands Antilles",
				"New Caledonia", "New Zealand", "Nicaragua", "Niger",
				"Nigeria", "Niue", "Norfolk Island",
				"Northern Mariana Islands", "Norway", "Oman", "Pakistan",
				"Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru",
				"Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico",
				"Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda",
				"Saint Kitts and Nevis", "Saint Lucia",
				"Saint Vincent and the Grenadines", "Samoa", "San Marino",
				"Sao Tome and Principe", "Saudi Arabia", "Senegal",
				"Seychelles", "Sierra Leone", "Singapore",
				"Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands",
				"Somalia", "South Africa",
				"South Georgia and the South Sandwich Islands", "Spain",
				"Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan",
				"Suriname", "Svalbard and Jan Mayen Islands", "Swaziland",
				"Sweden", "Switzerland", "Syrian Arab Republic",
				"Taiwan, Province of China", "Tajikistan",
				"Tanzania, United Republic of", "Thailand", "Togo", "Tokelau",
				"Tonga", "Trinidad and Tobago", "Tunisia", "Turkey",
				"Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda",
				"Ukraine", "United Arab Emirates", "United Kingdom",
				"United States", "United States Minor Outlying Islands",
				"Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam",
				"Virgin Islands (British)", "Virgin Islands (U.S.)",
				"Wallis and Futuna Islands", "Western Sahara", "Yemen",
				"Yugoslavia", "Zambia", "Zimbabwe");
		render(user, countries);
	}

	public static void editDetails(String firstName, String lastName,
			String email, String password, String addressLine1,
			String addressLine2, String city, String state, String country,
			String postalCode, String house, int age) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.firstName = firstName;
		user.lastName = lastName;
		user.email = email;
		user.password = password;
		user.addressLine1 = addressLine1;

		user.house = house;
		user.age = age;
		user.save();
		Logger.info("Details changed to: " + firstName + lastName + email
				+ password + addressLine1 + addressLine2 + city + state
				+ country + postalCode + house + age);
		index();
	}

	public static void editFirstName(String firstName) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.firstName = firstName;
		Logger.info("First Name changed to: " + firstName);
		index();
	}

	public static void editLastName(String lastName) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.firstName = lastName;
		Logger.info("Last Name changed to: " + lastName);
		index();
	}

	public static void editEmail(String email) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.email = email;
		Logger.info("Email changed to: " + email);
		index();
	}

	public static void editPassword(String password) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.password = password;
		Logger.info("Password changed to: " + password);
		index();
	}

	public static void editAddress(String addressLine1, String addressLine2,
			String city, String state, String country, String postalCode) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.addressLine1 = addressLine1;
		user.addressLine2 = addressLine2;
		user.city = city;
		user.state = state;
		user.country = country;
		user.postalCode = postalCode;
		Logger.info("Address changed to: " + addressLine1 + addressLine2 + city
				+ state + country + postalCode);
		index();
	}

	public static void editHouse(String house) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.house = house;
		Logger.info("House changed to: " + house);
		index();
	}

	public static void editAge(int age) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.age = age;
		Logger.info("Age changed to: " + age);
		index();
	}
}