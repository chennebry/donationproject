package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;

@Entity
public class Donation extends Model {
	public long received;
	public String methodDonated;

	@ManyToOne
	public User from;

	@ManyToOne
	public Candidate to;

	public Donation(User from, long received, String methodDonated, Candidate to) {
		this.from = from;
		this.received = received;
		this.methodDonated = methodDonated;
		this.to = to;
	}
}