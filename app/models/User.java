package models;

import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import play.db.jpa.Model;

@Entity
public class User extends Model {
	public boolean ukCitizen;
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public int age;
	public String addressLine1;
	public String addressLine2;
	public String city;
	public String state;
	public String country;
	public String postalCode;
	public String house;

	public User(boolean ukCitizen, String firstName, String lastName,
			String email, String password, int age, String addressLine1,
			String addressLine2, String city, String state, String country,
			String postalCode, String house) {
		this.ukCitizen = ukCitizen;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.age = age;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
		this.house = house;
	}

	public static User findByEmail(String email) {
		return find("email", email).first();
	}

	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}
}