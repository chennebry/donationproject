package models;

import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import play.db.jpa.Model;

@Entity
public class Candidate extends Model {
	public String firstName;
	public String lastName;
	public String office;
	public String message;
	public long target;

	public Candidate(String firstName, String lastName, String office,
			String message, long target) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.office = office;
		this.message = message;
		this.target = target;
	}

}